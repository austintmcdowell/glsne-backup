import os
import numpy as np
import glob


def main():

    r_files = glob.glob("results_*")

    r_files = r_files[106:]

    for r_file in r_files:
        
        print 'working on ' + r_file 
        
        read_results( r_file )
        

def read_results( r_file ):
# start at index 106
    results = open( r_file )

    start = r_file.find('_')+1

    name = r_file[start:]

    r = results.readlines()

    images = []
    for result in r:
        line = result.split()
        for entry in line:
            if (entry.find('scie') != -1) and (entry.find('.fits') != -1):
                images.append( entry )

    #return images

    i = 0
    for image in images:

        #start = image.find('v1/')+3

        #image = image[start:]

        #return image

        bashCommand = 'wget --load-cookies=ptf.txt -erobots=off "http://irsa.ipac.caltech.edu/ibe/data/ptf/images/level1/'+image+'" -O /global/cscratch1/sd/amcd/images/'+name+'_dir/'+name+'_image_'+str(i)+'.fits'
      
        #return bashCommand

        os.system(bashCommand)
      
        i += 1


main()
