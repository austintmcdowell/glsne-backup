import glob
import os

def main():

    dirs = glob.glob("*_dir")

    for d in dirs:
        os.chdir( '/global/cscratch1/sd/amcd/images/'+d )
        fits_files = glob.glob("*.fits")
        
        for ff in fits_files:

            start = ff.find('_image')
            end = ff.find('.fits')

            bashCommand = 'sex -c ../default.sex -CATALOG_NAME se_catalog_'+ff[start+7:end]+'.cat '+ff
            os.system( bashCommand )


def read_lens():

    lst = open('../masterlens1.txt')

    l = lst.readlines()

    names = []
    for i in xrange(len(l)):

        names.append( l[i].split()[0] )


    return names

main()
